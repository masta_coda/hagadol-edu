<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Apply Now - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Apply Now!</h1>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li class="active">Apply Now</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-3">
                <?php include "apply_sidebar.php"; ?>
            </div>

            <div class="col-md-9">
                <h2>Application Procedure</h2>
                <img src="img/apply.jpg" class="img-responsive" alt="Image of application form">

                <br>

                <h3>Step 1</h3>
                <p>Visit our offices at <a href="contact.php">1<sup>st</sup> Floor, 82 Mutare Road in Msasa</a>, fill in an application form and pay an application fee of only $50.</p>

                <h3>Step 2</h3>
                <p>Submit the completed application form together with the following documents: (originals will be scanned and returned to you)</p>
                <ul>
                    <li>Original Ordinary and/or Advanced Level results (both ZIMSEC & Cambridge accepted)</li>
                    <li>Original Passport</li>
                    <li>Original National ID card</li>
                    <li>One passport sized photo</li>
                </ul>

                <h3>Step 3</h3>
                <p>Once all documents have been recieved, we will forward them to Sharda University and the response will be ready within 7 working days.</p>

                <h3>Step 4</h3>
                <p>If your application has been successful, you will be issued a letter of acceptance.</p>

                <h3>Step 5</h3>
                <p>You will be required to pay a non-refundable assurity deposit of $500 to secure your place at the university</p>

                <h3>Step 6</h3>
                <p>Apply for an Indian student Visa. Hagadol Education has an established relationship with the Indian Embassy in Zimbabwe and will assist in this process.</p>

                <h3>Step 7</h3>
                <p>Bon Voyage!</p>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
