<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>Postgraduate Courses</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="sharda.php">Sharda University</a>
                    </li>
                    <li class="active">Postgraduate Courses</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>Postgraduate Courses</h2>

                <img src="img/postgrad.jpg" class="img-responsive" alt="Post Graduate courses">

                <br>

                <p>Sharda University offers the latest and continuously upgraded industry endorsed curriculum in line with the best practices in U.S. and U.K. Universities. The fully simulated instructional material developed at the university and delivered through latest audio visual aids within the classroom by experienced as well as young global faculty.</p>

                <hr>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Dietetics & Public Health Nutrition</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Dip. in Dietetics & Public Health Nutrition</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Business Studies</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>MBA-Human Resource Management</li>
                                    <li>MBA (With Education foreign Tour)-Dual Specialization</li>
                                    <li>MBA - Health Care & Hospital Administration</li>
                                    <li>MBA (Without Education Foreign Tour)-Dual Specialization</li>
                                    <li>MBA - Marketing Management</li>
                                    <li>MBA - Banking & Finance</li>
                                    <li>MBA - International Business</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Clinical Research</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>PG Dip. in Clinical Research</li>
                                    <li>M. Sc. Clinical Research</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Computer Applications</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>MCA</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Dental</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>M.D.S-Pedodontics and Preventive Dentistry</li>
                                    <li>MDS (Oral Pathology & Microbiology)</li>
                                    <li>MDS (Oral & Maxillofacial Surgery)</li>
                                    <li>MDS (Conservative Dentistry & Endodontics)</li>
                                    <li>MDS (Oral & Maxillofacial Surgery)</li>
                                    <li>MDS (Oral Pathology & Microbiology)</li>
                                    <li>MDS (Orthodontics & Dentofacial Orthopedics)</li>
                                    <li>MDS (Periodontology)</li>
                                    <li>MDS (Prosthodontics and Crown & Bridge)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Engineering</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>M.Tech (Polymers and Plastics Technology)</li>
                                    <li>M.Tech (Material Technology)-Nanotechnology</li>
                                    <li>M. Tech. in Atmospheric Sciences</li>
                                    <li>M.Tech (Biotechnology)-Animal Biotechnology</li>
                                    <li>M.Tech in Computer Science & Engineering | Software Engineering</li>
                                    <li>M.Tech in Civil Engineering with specialization in Environmental Engineering</li>
                                    <li>M.Tech (EEE)-Power Systems Engineering</li>
                                    <li>M.Tech (Microwave Technology)</li>
                                    <li>M.Tech (Material Technology)-Polymer Science & Technology</li>
                                    <li>M.Tech. in Renewable Energy</li>
                                    <li>M.Tech (Biotechnology)-Plant Tissue Culture</li>
                                    <li>M.Tech in Civil Engineering with specialization in Structural Engineering</li>
                                    <li>M.Tech (Computer Science & Engineering)-Networking</li>
                                    <li>M.Tech (V.L.S.I. Technology)</li>
                                    <li>M.Tech (EEE)-Instrumentation and Controls</li>
                                    <li>M. Tech(Mechanical Engineering)-Thermal and Fluids Engineering</li>
                                    <li>M. Sc. Biotechnology</li>
                                    <li>M.Tech in Civil Engineering with specialization in Water Resources</li>
                                    <li>M.Tech in Civil Engineering with specialization in Environmental Resources Engineering and Management</li>
                                    <li>M.Tech in Civil Engineering with specialization in Infrastructure Engineering and Management</li>
                                    <li>M.Tech in Civil Engineering with specialization in Geo-Informatic Engineering</li>
                                    <li>M.Tech in Civil Engineering with specialization in Geotechnical Engineering</li>
                                    <li>M.Tech. in CSE with specialization in Mobile Computing</li>
                                    <li>M.Tech. in CSE with specialization in Cloud Computing</li>
                                    <li>M. Tech (Mechanical Engineering)-Production and Industrial Engineering</li>
                                    <li>M.Tech. in ECE with specialization in Optoelectronics & Laser Technology</li>
                                    <li>M.Tech. in CSE with specialization in Web Technologies</li>
                                    <li>M. Tech (Mechanical Engineering)-Machine Design</li>
                                    <li>M.Tech. in ECE with specialization in Optical Communication</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Foreign Language</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>M.A (English)- Specialization in English Teaching</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Law</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>LLM - Corporate Law</li>
                                    <li>LLM - International Law</li>
                                    <li>LLM - Energy Law</li>
                                    <li>LLM - Criminal Law</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Mass Communication</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>MA (Journalism & Mass Comm.)</li>
                                    <li>PG Diploma in Advertising & Public Relations</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Medical</a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Master of Surgery (Anatomy)</li>
                                    <li>Doctor of Medicine(Physiology)</li>
                                    <li>Biochemistry</li>
                                    <li>MD - Microbiology</li>
                                    <li>Doctor of Medicine (Pathology)</li>
                                    <li>Doctor of Medicine (Pharmacology)</li>
                                    <li>MD- Community Medicine</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Physiotherapy</a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>M. Sc. (Medical Physiology)</li>
                                    <li>M. Sc. (Medical Microbiology)</li>
                                    <li>M. Sc. (Medical Anatomy)</li>
                                    <li>M. Sc. (Medical Bio-Chemistry)</li>
                                    <li>M. Sc. (Medical Pharmacology)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.panel-group -->
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
