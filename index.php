<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('img/diploma.jpeg');"></div>
                <div class="carousel-caption">
                <span class="colored-carousel-caption">
                    <h2>Hagadol Education</h2>
                    <div></div>
                    <h4>Hagadol Education is an education consultancy which helps students from Southern Africa to apply and enroll at world class Universities in India.</h4>
                </span>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/campus1.jpg');"></div>
                <div class="carousel-caption">
                <span class="colored-carousel-caption">
                    <h2>Sharda University</h2>
                    <div></div>
                    <h4>Sharda is a leading University based in Greater Noida, Delhi NCR. Sharda has established itself as a high quality education provider with prime focus on holistic learning and imbibing competitive abilities in students.</h4>
                    <div></div>
                </span>
                    <a href="sharda.php" class="btn btn-success">Learn More</a>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/mumbai.jpg');"></div>
                <div class="carousel-caption">
                <span class="colored-carousel-caption">
                    <h2>India</h2>
                    <div></div>
                    <h4>Indian tertiary institutions are world renowned for educational excellence, particularly in the fields of Science, Engineering, Medicine and Information Technology.</h4>
                </span>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Welcome to Hagadol Education
                </h1>

                <br>

                <p>Hagadol Education is a unique independent education agency which helps students from Southern Africa to apply and enroll at Universities in India. These universities are world class and internationally recognized.
</p>

                <p>The main university is Sharda University which offers you the best learning environment and resources needed to enjoy your studies and become successful. Established in 1996 Sharda has become a well-known global center of learning that promotes innovation and professional excellence. Its campus stretches across 65 acres in Greater Noida, Agra and Mathura in Dehli-NCR. Not only is Sharda a great choice of school but it also has affordable tuition fees starting from as low as USD 2000 as well as affordable accommodation rates. Sharda was awarded the best private university for 4 years in a row. There are over 150 course to choose from in various fields of study that include: Engineering and Technology; Business Studies; Dental Sciences; Medical Sciences and Research; Arts, Design and Media Studies and many others.</p>

                <p>The university hand-holds students with placement assistance in organizations around the world. Sharda also has its own hospital that carries roughly 900 beds. The university invested over 2 million United States Dollars in software and IT solutions, putting it at the heart of medical and ICT related innovations. A university that helps you reach beyond boundaries. Apply Now!</p>

                <br>

            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-graduation-cap"></i> Sharda University</h4>
                    </div>
                    <div class="panel-body">
                        <p>Sharda University is a leading tertiary institution based near the Indian capital of Delhi.The University has established itself as a high quality education provider with prime focus on holistic learning and imbibing competitive abilities in students.</p>
                        <a href="sharda.php" class="btn btn-success">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-paper-plane-o"></i> Apply Now</h4>
                    </div>
                    <div class="panel-body">
                        <p>Hagadol Educations trained Education consultants assist you throughout the entire application process. From the initial application through to the visa application process, we are here for you! </p>
                        <a href="apply.php" class="btn btn-success">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">India - Gateway to the East</h2>
            </div>
            <div class="col-md-6">
                <h3><u>Why study at Sharda?</u></h3>
                <ul style="font-size:1.3em;">
                    <li>Exchange programs with 80+ universities from the US, UK and Canada etc.</li>
                    <li>Annual recruitment conducted by over 180 top companies including IBM, Johnson & Johnson, Audi and many more</li>
                    <li>With students from over 46 countries, Sharda has a culturally diverse environment providing students with global exposure.</li>
                    <li>Low costs of living</li>
                    <li>English widely spoken</li>
                    <li>Experience different cultures</li>
                    <li>900 bed multi-specialty hospital for medical students</li>
                    <li>Well known IT & medical hub</li>
                    <li>120+ faculty members with PhD's and foreign university experience</li>

                </ul>
                <p>For many decades, India has experienced phenomenal economic growth averaging 5.83% from 1951 - 2014. The Government of India has invested this wealth back into their people through generous funding for their education system, especially at the tertiary level. This has catapulted Indian education institutions onto the world stage especially in the fields of Science, Technology, Engineering, Mathematics and Medicine</p>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="img/tajmahal.jpg" alt="">
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->
        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p>What are you waiting for? Apply now and secure a place in a world class University today!</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="apply.php">Apply Now!</a>
                </div>
            </div>
        </div>

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
