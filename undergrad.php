<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>Undergraduate Courses</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="sharda.php">Sharda University</a>
                    </li>
                    <li class="active">Undergraduate Courses</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>Undergraduate Courses</h2>

                <img src="img/undergrad.jpg" class="img-responsive" alt="undergraduate courses image">

                <br>

                <p>Sharda University offers the latest and continuously upgraded industry endorsed curriculum in line with the best practices in U.S. and U.K. Universities. The fully simulated instructional material developed at the university and delivered through latest audio visual aids within the classroom by experienced as well as young global faculty.</p>

                <hr>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Architecture & Planning</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>B.Arch</li>
                                    <li>Bachelor in Planning (B.Plan)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Basic Sciences</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BS-MS (Integrated)- Physics</li>
                                    <li>BS-MS (Integrated)- Chemistry</li>
                                    <li>BS-MS (Integrated)- Mathematics</li>
                                    <li>BS-MS (Integrated)- Biological Sciences</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Business Studies</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BBA Dual Specialization (With Education Foreign Study Tour)</li>
                                    <li>BBA Dual Specialization (Without Education Foreign Study Tour)</li>
                                    <li>BBA-Banking & Finance</li>
                                    <li>BBA - Entrepreneurship</li>
                                    <li>BBA-International Business</li>
                                    <li>B. Com. (Hons)</li>
                                    <li>B.A (Hons)-Applied Economics</li>
                                    <li>Certificate course(Industry Linked) in Retail Management</li>
                                    <li>Certificate course(Industry Linked) in Rural Business</li>
                                    <li>Certificate course(Industry Linked) in Business Anylatics</li>
                                    <li>Certificate course(Industry Linked) in Logistics & Supply chain Management</li>
                                    <li>Certificate course(Industry Linked) in Communication Management</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Fine Art</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Bachelor of Fine Arts</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Computer Applications</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BCA</li>
                                    <li>BCA (Software Engineering)</li>
                                    <li>BCA (E.Commerce)</li>
                                    <li>BCA (E.Governance)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Dental</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BDS (through UPCAT- Dental)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Design</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Bachelor of Design (B.Des.)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Engineering</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>B.Tech (CSE)</li>
                                    <li>B.Tech (IT)</li>
                                    <li>B.Tech (ECE)</li>
                                    <li>B.Tech (Electrical & Electronics)</li>
                                    <li>B.Tech (Mechanical)</li>
                                    <li>B.Tech (Electronics & Instrumentation)</li>
                                    <li>B.Tech (Civil)</li>
                                    <li>B. Tech (BioTech)</li>
                                    <li>B.Tech (Automobile)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Foreign Languages</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BA (Hons.) - English</li>
                                    <li>BA (Hons.) - German</li>
                                    <li>BA (Hons.) - French</li>
                                    <li>BA (Hons.) - Spanish</li>
                                    <li>Certificate course in English, French, Spanish, German, Chinese</li>
                                    <li>B.A.(Hons.) - Chinese</li>
                                    <li>B.A.(Hons.)- Hindi</li>
                                    <li>Diploma in English</li>
                                    <li>B.A - Music</li>
                                    <li>B.A-Dance</li>
                                    <li>B.A-Painting</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Law</a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BBA LLB (Five Year Integrated Course)</li>
                                    <li>B.Com LLB (Five Year Integrated Course)</li>
                                    <li>BA LLB (Five Year Integrated Course)</li>
                                    <li>B.Sc. LLB (Five Year Integrated Course)</li>
                                    <li>B.Tech LLB (Six Year Integrated Course)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse11">Mass Communication</a>
                            </h4>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>BA (Journalism &Mass Comm.)</li>
                                    <li>BA (Journalism & Mass Comm.) in Hindi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1213">Medical</a>
                            </h4>
                        </div>
                        <div id="collapse1213" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>M.B.B.S</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse13">Medical Lab Technology</a>
                            </h4>
                        </div>
                        <div id="collapse13" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>B. Sc. - Medical Lab Technology</li>
                                    <li>B.Sc -Medical Imaging Technology</li>
                                    <li>Diploma in Medical Lab technology (DMLT)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse14">Nursing</a>
                            </h4>
                        </div>
                        <div id="collapse14" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>General Nursing & Midwifery (GNM)</li>
                                    <li>B.Sc. (Nursing)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse15">Operations Theater Technology</a>
                            </h4>
                        </div>
                        <div id="collapse15" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Diploma in Operation Theater Technology</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse16">Physiotherapy</a>
                            </h4>
                        </div>
                        <div id="collapse16" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>Bachelor of Physiotherapy</li>
                                    <li>BSc-yoga & Naturopathy</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.panel-group -->
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
