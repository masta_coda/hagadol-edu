<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>Campus Life</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="sharda.php">Sharda University</a>
                    </li>
                    <li class="active">Campus Life</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>Campus Life</h2>

                <img src="img/campus1.jpg" class="img-responsive" alt="Sharda University Campus">

                <br>

                <div class="row">
                    <div class="col-md-9">
                        <h4><strong>Campus Life @ Sharda</strong></h4>
                        <p>We at Sharda University understand the ‘real world’ that awaits you once you walk out of the hallowed gates of the university, will need you to constantly reinvent yourself for handling new situations, sometimes new technology, new solutions, new people. For this, we host an environment for our students, which prepare them to become not only accomplished professionals but also well-rounded, compassionate, and exuberant adults, who are ready for the constantly evolving world.
    Sharda University’s campus life is a melting pot of various cultures, regions, countries, ethnicities, flavors, cuisines, sights, and sounds that has already enthralled thousands of students. The list below gives just a glimpse of various facets of campus life at Sharda University. But to truly feel the palpable sense of excitement, to see the convergence of global cultures in motion, and to experience a campus life truly ‘Beyond Boundaries’ one has to be here as these feelings cannot be described in words.</p>

                        <hr>
                    </div>

                    <div class="col-md-3">
                        <img src="img/campus.jpg" class="img-responsive img-thumbnail">
                    </div>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-9">
                        <h3><strong>Infrastructure and Facilities</strong></h3>
                        <p>We at Sharda University understand the ‘real world’ that awaits you once you walk out of the hallowed gates of the university, will need you to constantly reinvent yourself for handling new situations, sometimes new technology, new solutions, new people. For this, we host an environment for our students, which prepare them to become not only accomplished professionals but also well-rounded, compassionate, and exuberant adults, who are ready for the constantly evolving world.
    Sharda University’s campus life is a melting pot of various cultures, regions, countries, ethnicities, flavors, cuisines, sights, and sounds that has already enthralled thousands of students. The list below gives just a glimpse of various facets of campus life at Sharda University. But to truly feel the palpable sense of excitement, to see the convergence of global cultures in motion, and to experience a campus life truly ‘Beyond Boundaries’ one has to be here as these feelings cannot be described in words.</p>
                        <br>
                        <br>
                        <br>

                        <hr>
                    </div>

                    <div class="col-md-3">
                        <img src="img/sport.jpg" class="img-responsive img-thumbnail">
                        <img src="img/infra.jpg" class="img-responsive img-thumbnail">
                        <img src="img/hostel.jpg" class="img-responsive img-thumbnail">
                    </div>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-9">
                        <h3><strong>Eateries and Hang out points</strong></h3>
                        <p>We let our students enjoy the fast paced metro lifestyle right within the campus. Sagar Ratna, Amul Milk Parlor, Bikano, Café Coffee Day, Nescafe, Buddy run their outlets in the campus. In addition to various food outlets there is a rooftop Food Court.
Outside campus, there are many places you can visit after classes to unwind. At Jagat farms market complex, Alpha Commercial and Ansal Plaza, INOX Mall you get everything right from mouth-watering street food to daily shopping needs to books to go-karting experiences. There are some good restaurants too that serves most cuisines. Plus, Greater Noida is the heaven for morning joggers. The plush green and empty roads make the best place for cycling enthusiast</p>

                        <hr>
                    </div>

                    <div class="col-md-3">
                        <img src="img/food.jpg" class="img-responsive img-thumbnail">
                    </div>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-9">
                        <h3><strong>The World on One campus</strong></h3>
                        <p>Sharda University has students from over 42 countries and has thus become a medley of diverse cultures, backgrounds, and ethnicities. In an endeavor to make the most of the cultural diversity here, students have formed a “Cultural Diversity Team Initiative” under the mentorship of Dr. Daleep Parimoo, Asst. Professor and Head of Recruitment, Prof. Milindo Chakrabarti and Ms. Shanti Narayan.

Under the Cultural Diversity Team Initiative, students have enjoyed organizing some ‘Arabian Mehendi Design’ camps and ‘African Hair Plaiting’ camps. These two camps attracted hundred-odd entrants. We also celebrate their festivals here in campus like Easter celebration. More such programs are being planned.

Our students realize and believe that “human beings are great pools of intellectual ability and strength of character, and instead of working as islands; together we can achieve more.”

“For global excellence - we must be able to sustain global relationships.”
Your list of International friends is forever expanding</p>

                        <hr>
                    </div>

                    <div class="col-md-3">
                        <img src="img/itlab.jpg" class="img-responsive img-thumbnail">
                        <img src="img/this.jpg" class="img-responsive img-thumbnail">
                    </div>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <h3><strong>Credit Based Learning</strong></h3>
                        <p>The entire curriculum in the University has been planned in a highly flexible credit based system approach by studying the best practices. We have devised a term based system in which an academic year is broken into two 16 weeks terms and a summer term of 9 weeks.
Every term the student will have to take a minimum number of credits and get grades which are more than the minimum prescribed levels. The student will have choice to take only those courses which he feels would be used in his professional career. The student also will have option to drop a course after 2 weeks of start of the term, if he feels that the course doesn’t offer him the requisite skills that will be useful for his professional career, he may opt for another course during the term. All such academic mentoring would be possible with the help of Faculty advisors, who will be available 24x7 to counsel students for the right choice and variety of courses.</p>
                    </div>

                    <hr>

                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <h3><strong>Co-Curricular Activities</strong></h3>
                        <p><strong>Chorus:</strong> Chorus is the annual mega-cultural festival of Sharda University. A delicious cocktail of cultural diversity, social awareness and unending entertainment, by offering a rich blend of dance, music, arts, workshops and quizzes. Chorus over the past two years has witnessed mesmerizing performances by popular singers such as Vani kapoor, Om Puri, Sunil Grover, Sonu Nigam, Simon Webb- Blue, UK, Jal- The Band, and Hard Kaur. Moreover, the festival had offered stellar performances by giants of entertainment industry such as Rannvijay, Raghu and Rajiv, Roadies host and VJ on MTV, who hosted the famous reality show on the campus.</p>
                    </div>

                    <hr>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-9">
                        <p><strong>Clubs/ Societies:</strong> Our students discover their talent by participating in diverse forums, clubs, leadership workshops and various other events such as Olympiads, Quizzes. We have a Student Centre, under which several clubs and societies such as Photography, Dance, Literature, Music, and Dramatics are functional round the year with periodic inter-and intra-institutional events and workshops.
As a student, you can also be an active member of a society that will help you to bring out your true potential, aptitude, and give your career a perfect direction</p>
                    </div>

                    <div class="col-md-3">
                        <img src="img/event.jpg" class="img-responsive img-thumbnail"></img>
                    </div>

                    <hr>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <p><strong>Kartavya club at Sharda University:</strong> Kartavya is a Non-Governmental Organization providing free and high quality education to underprivileged children living in slums and villages in and around Sharda University campus. The whole management of the organization is handled by a group of students from university. The students take time out from their busy schedules and teach underprivileged children from the society. Most of these children come from slums in the neighborhood and their parents are mostly working as laborers at various construction sites in Greater Noida.</p>
                    </div>

                    <hr>
                </div>
                <!-- /.row -->

                <!-- row -->
                <div class="row">
                    <div class="col-md-9">
                        <p><strong>Campus Magazine: The Shardans</strong>  is a fortnightly practice paper produced by the students of Mass Communication. It’s neither a newspaper, nor a journal but would fit somewhere between a periodical and a newsletter. The idea behind the launch of The Shardans was to give students a platform to hone their skills, where they can identify news stories, write reports and features, edit copy, give headlines, do layout and design and learn news and feature photography. Our idea is to give them the feel of a professional even when they are at the learning stage. We can humbly say that to a great extent we have succeeded in our endeavor. We have developed a small but dedicated team which can be an asset for any news organization.</p>
                    </div>

                    <div class="col-md-3">
                        <img src="img/media.jpg" class="img-responsive img-thumbnail"></img>
                    </div>

                    <hr>
                </div>
                <!-- /.row -->

                
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
