<div class="list-group">
    <a href="sharda.php" class="list-group-item">About Sharda University</a>
    <a href="undergrad.php" class="list-group-item">Undergraduate Courses</a>
    <a href="postgrad.php" class="list-group-item">Post Graduate Courses</a>
    <a href="doctoral.php" class="list-group-item">Doctoral Courses</a>
    <a href="campuslife.php" class="list-group-item">Campus Life</a>
    <a href="opportunities.php" class="list-group-item">Opportunities after graduation</a>
    <a href="faq.php" class="list-group-item">Frequently Asked Questions</a>
</div>