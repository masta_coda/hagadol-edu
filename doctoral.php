<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>Doctoral Courses</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="sharda.php">Sharda University</a>
                    </li>
                    <li class="active">Doctoral Courses</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>Doctoral Courses</h2>

                <img src="img/doctorate.jpg" class="img-responsive" alt="doctorate">

                <br>

                <p>Sharda University offers the latest and continuously upgraded industry endorsed curriculum in line with the best practices in U.S. and U.K. Universities. The fully simulated instructional material developed at the university and delivered through latest audio visual aids within the classroom by experienced as well as young global faculty.</p>

                <hr>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Basic Sciences</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>PhD - Physics</li>
                                    <li>PhD - Chemistry</li>
                                    <li>PhD - Mathematics</li>
                                    <li>PhD - Bio Technology</li>
                                    <li>PhD - Environmental Science</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Business Studies</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>PhD - Finance Management</li>
                                    <li>PhD - International Business</li>
                                    <li>PhD - Business Economics</li>
                                    <li>PhD - Human Resources</li>
                                    <li>PhD - Marketing & Brand Management</li>
                                    <li>PhD - General Management</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Engineering</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>PhD - Electronics & Communication Engineering</li>
                                    <li>PhD - Electrical Engineering</li>
                                    <li>PhD - Computer Science Engineering</li>
                                    <li>PhD - Civil Engineering</li>
                                    <li>PhD - Environmental Engineering</li>
                                    <li>PhD - Mechanical Engineering</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Law</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>PhD in Law</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.panel-group -->
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
