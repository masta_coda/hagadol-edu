<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Hagadol Education 2012 - <?php echo date("Y")?></p>
        </div>
    </div>
</footer>
<!-- /Footer -->