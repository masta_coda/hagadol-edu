<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>Opportunities</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="sharda.php">Sharda University</a>
                    </li>
                    <li class="active">Placement opportunities after graduation</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>Placement Opportunities</h2>

                <img src="img/placement.jpg" class="img-responsive" alt="Work placements">

                <br>

                <h4><strong>Placement Milestones</strong></h4>
                <p>Our alumni are the prized testimony of the quality professional education that we impart. We take great pride in notifying that since the year 2000 more than 15,000 students have been placed in many reputed organizations like IBM, Cognizant, Mphais, Global Logic, Tech Mahindra, NIIT Technologies, BirlaSoft, Steria India, Adani Wilmar, JK Technosoft, NTL Electronics, Royal Bank of Scotland (RBS), Vodafone, ICICI Securities, Laurent & Benon, Birlasoft India Ltd., etc. Our students continue to make us proud every day through the contribution they make at senior executive positions in thesleading organizations across the world.</p>

                <p>The placement process at Sharda Univesrsity leap-frog a student’s career. Through our meticulously organized placement process, the kind of positions which are offered by leading global giants to our students goes out to prove the mettle of the students, and of the University. The increasing trust and confidence displayed by these organizations in our students exemplifies the growing confidence in the University among the corporate fraternity.</p>

                <br>

                <h4><strong>Some Recruiters of the B.Tech Program</strong></h4>
                <p>NIIT Technologies, Tech Mahindra, NTT Data, Global Logic, Cognizant, IBM India, Steria India, NIIT Technologies, Mphasis, R-Systems, Safenet, AIS Glass, Compro Technologies, Amara Raja Batteries, 3G Systems, Interglobe Technologies, JK Technosoft, United Spirits (UB Group), Lal Path Labs, Adani Wilmar, Unisys etc. to name a few.</p>

                <br>

                <h4><strong>Some Recruiters of the MBA Program</strong></h4>
                <p>The Royal Bank of Scotland (RBS), Winspire Consulting Pvt. Ltd, I Act Global, Birlasoft India Ltd, Vodafone, ICICI Securities Ltd, Laurent & Benon, Payportal Services Pvt. Ltd, MSL Learning Systems, Traders Factory Academy, Brand Connect India, Questa Management Consulting Pvt. Ltd, Capital Via, Agencyconnect.com, Oerlikon Graziano, etc to name a few.</p>

                <hr>

                <!-- Our Partners -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Top Recruiters for 2013-14</h2>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/tata.jpg" alt="">
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/ntt.jpg" alt="">
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/mahindra.jpg" alt="">
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/ibm.jpg" alt="">
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/niit.jpg" alt="">
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <img class="img-responsive customer-img" src="img/hcl.jpg" alt="">
                    </div>
                </div>
                <!-- /Our Partners -->

                
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
