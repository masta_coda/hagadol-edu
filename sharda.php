<!DOCTYPE html>
<html lang="en">

<head>

    <?php include "meta.php"; ?>

    <title>Sharda University - Hagadol Education</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <?php include "nav.php"; ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sharda University <small>About</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li class="active">Sharda University</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Sidebar Column -->
            <div class="col-md-3">
                <?php include "sidebar.php";?>
            </div>
            <!-- Content Column -->
            <div class="col-md-9">
                <h2>About Sharda University</h2>
                <img src="img/sharda.jpg" class="img-responsive" alt="Sharda University Students">

                <br>

                <p>Sharda University is a leading Educational institution based out of Greater Noida, Delhi NCR. A venture of the renowned SGI group, the University has established itself as a high quality education provider with prime focus on holistic learning and imbibing competitive abilities in students.</p>

                <p>The University is approved by UGC and prides itself in being the only multi-discipline campus in the NCR, spread over 63 acres and equipped with world class facilities.</p>

                <p>Sharda University promises to become one of the India's leading universities with an acknowledged reputation for excellence in research and teaching. With its outstanding faculty, world class teaching standards, and innovative academic programmes, Sharda intends to set a new benchmark in the Indian education system.</p>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <?php include "footer.php"; ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
